import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { ApiService } from './services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'FirstProject';
  displayedColumns: string[] = [ 'productName', 'category', 'date','prodlist','price', 'comment', 'action'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private dialog: MatDialog, private api: ApiService) {

  }
  ngOnInit(): void {
    this.getAllproducts();
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {

      width: '30%'
    }).afterClosed().subscribe(val=>{
      if(val==='save'){
        this.getAllproducts();
      }
    })
  }
  getAllproducts(){
      this.api.getproduct()
      .subscribe({
        next:(res)=>{
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
        },
        error:(err)=>{
          alert("Alert while fetching records")
        }
      })
  }
    editProducts(element:any){
      this.dialog.open(DialogComponent, {
        width:'30%',
        data:element
      }).afterClosed().subscribe(val=>{
        if(val==='update'){
          this.getAllproducts();
        }
      })
    }

    deleteProduct(id:any){
      this.api.deleteProduct(id)
      .subscribe({
        next:(res)=>{
          alert("Product deleted Successfully!!");
          this.getAllproducts();
        },
        error:()=>{
          alert("Error while deleting product");
        }
      })
    }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



}
